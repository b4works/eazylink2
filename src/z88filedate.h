#ifndef Z88FILEDATE_H
#define Z88FILEDATE_H

#include <QDateTime>

// This is a wrapper and utility class to be used for interchanging of
// Z88 create & update stamps to/from host file, via QDateTime objects
class Z88FileDate
{
public:
    Z88FileDate(QString z88CreateDate, QString z88ModifiedDate);
    static QDateTime toHostDate(QString z88FileDate);
    static QString fromHostDate(QDateTime hostFileDate);
    QDateTime getCreatedTimestamp() const;
    QDateTime getUpdatedTimestamp() const;

private:
    QDateTime createdTimestamp;
    QDateTime updatedTimestamp;
};

#endif // Z88FILEDATE_H
