/*********************************************************************************************

 EazyLink2 - Fast Client/Server Z88 File Management
 Copyright (C) 2012-2015 Gunther Strube (gstrube@gmail.com) & Oscar Ernohazy

 EazyLink2 is free software; you can redistribute it and/or modify it under the terms of the
 GNU General Public License as published by the Free Software Foundation;
 either version 2, or (at your option) any later version.
 EazyLink2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 See the GNU General Public License for more details.
 You should have received a copy of the GNU General Public License along with EazyLink2;
 see the file COPYING. If not, write to the
 Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

**********************************************************************************************/

#include <QListIterator>
#include <QIcon>
#include <QMimeData>
#include <QDragEnterEvent>
#include <QUrl>
#include <QFileInfo>
#include "z88_devview.h"
#include "z88storageviewer.h"


/**
  * the Z88 File Selection class constructor.
  * @param item is the Entry from the QTree.
  * @param dev_name is the Z88 Storage Device name, ie RAM.1
  */
Z88_Selection::Z88_Selection(QTreeWidgetItem *item, const QString &dev_name) :
    m_type(Z88_DevView::type_Dir),
    m_QtreeItem(item)
{
    if(m_QtreeItem){
        setRelFspec(m_QtreeItem->text(0));
    }

    setItemFspec(m_QtreeItem, dev_name);
}

/**
  * Set the Item's Filename Spec.
  */
const QString &Z88_Selection::setItemFspec(QTreeWidgetItem *item, const QString &devname)
{
    QString fname;
    m_fspec = devname + "/";

    bool rootdev = false;

    if(item){
        m_type = (item->type() == Z88_DevView::type_File) ? Z88_DevView::type_File : Z88_DevView::type_Dir;
    }
    else{
        rootdev = true;
        m_type = Z88_DevView::type_Dir;
    }

    /**
      * Build the Full filename by backing up to the top of the tree
      */
    while(item){
        fname.prepend(item->text(0));
        item = item->parent();
        if(!item)break;
        fname.prepend("/");
    }
    m_fspec += fname;

    /**
      * If the Selection is just the Z88 Storage device root,
      * Don't append a trailing '/'
      */
    if(!rootdev && m_type == Z88_DevView::type_Dir ){
        m_fspec += "/";
    }
    return m_fspec;
}

/**
  * The Z88 Device View Class Constructor.
  * @param devname is the name of the Z88 Storage Device. ie :RAM.1
  * @param com_thread is the communications thread to use for I/O.
  * @param parent is the owner QWidget.
  */
Z88_DevView::Z88_DevView(const QString devname, Z88Comms &com_thread, Z88StorageViewer *parent) :
    QTreeWidget((QWidget*)parent),
    m_cthread(com_thread),
    m_devname(devname),
    m_devSize(SZ_NOT_AVAIL),
    m_devFree(SZ_NOT_AVAIL),
    m_z88StorageView(parent),
    m_selChangeLock(false)
{
    setObjectName("Z88Tree");

    setColumnCount(4);

    QStringList hdrList;
    hdrList << "File or Directory" << "Size" << "Create Date" << "Update Date";

    setHeaderLabels(hdrList);
    setSortingEnabled (true);
    sortItems(0, Qt::AscendingOrder);
    setColumnWidth(0,207);
    setColumnWidth(1,60);
    setColumnWidth(2,110);
    setColumnWidth(3,110);

    setAcceptDrops(true);
    setAutoExpandDelay(600);

    setDragDropMode(QAbstractItemView::DragDrop);
    setAnimated(true);

    setSelectionMode(QAbstractItemView::ExtendedSelection);
}

/**
  * Get the Z88 Device name, ie :RAM.1
  */
const QString &Z88_DevView::getDevname()
{
    return m_devname;
}

/**
  * Insert a Unique Z88 Filename or Directory names into the Tree view.
  * @param fspec_list is split list of a single file or dir, broken into parts.
  *  example a file :RAM.1/dir1/foo.txt would be 3 entries in the list ":RAM.1" "dir1" "foo.txt"
  * @param d_type is the type of entry, dir or file.
  * @param fsize is the Size of the file, or blank if not enabled.
  * @param fcreate_date is the File Creation date, or leave blank if not enabled.
  * @param fmod_date is the File Modified date, or leave blank if not enabled.
  * @return true on success.
  */
bool Z88_DevView::insertUniqueFspec(QStringList &fspec_list, entryType d_type,
                                    const QString &fsize, const QString &fcreate_date,
                                    const QString &fmod_date)
{
    QStringList entry;

    if(!fspec_list.isEmpty()){
        /**
         * Sanity Check to make sure the files belong in this device display
         */
        if(fspec_list[0] != m_devname){
            return false;
        }

        /**
          * Strip the Device name
          */
        fspec_list.removeFirst();

        if(!fspec_list.isEmpty()){
            QList<QTreeWidgetItem *> treeItem;
            treeItem = findItems(fspec_list[0], Qt::MatchExactly, 0);

            /**
              * If the Dir or file is not already in the tree
              */
            if(treeItem.isEmpty()){
                entry << fspec_list[0];

               /**
                 * Only Append Time and date of a file, not a dir
                 */
                if(fspec_list.count() == 1){
                    if(!fsize.isEmpty()){
                        entry << fsize;
                    }
                    else{
                        entry << " ";
                    }

                    if(!fcreate_date.isEmpty()){
                        entry << fcreate_date;
                    }
                    else{
                        entry << " ";
                    }

                    if(!fmod_date.isEmpty()){
                        entry << fmod_date;
                    }
                    else{
                        entry << " ";
                    }
                }
                QTreeWidgetItem *qt = new QTreeWidgetItem(entry, d_type);

                /**
                  * Set the Icon for the file type
                  */
                set_EntryIcon(qt, fspec_list[0], d_type);

                addTopLevelItem(qt);

                fspec_list.removeFirst();

                if(fspec_list.isEmpty()){
                    return true;
                }
                return insertFspecList(qt, fspec_list, d_type, fsize, fcreate_date, fmod_date);
            }

            /**
              * Parent dir exists
              */
            fspec_list.removeFirst();

            if(!fspec_list.isEmpty()){
                return insertFspecList(treeItem[0], fspec_list, d_type, fsize, fcreate_date, fmod_date);
            }
        }
    }
    return false;
}

/**
  * Insert a Z88 Filename or Directory names into the Tree view.
  * @param parent is the QTreeWidgetItem to inser the Child after.
  * @param fspec_list is split list of a single file or dir, broken into parts.
  *  example a file :RAM.1/dir1/foo.txt would be 3 entries in the list ":RAM.1" "dir1" "foo.txt"
  * @param d_type is the type of entry, dir or file.
  * @param fsize is the Size of the file, or blank if not enabled.
  * @param fcreate_date is the File Creation date, or leave blank if not enabled.
  * @param fmod_date is the File Modified date, or leave blank if not enabled.
  * @return true on success.
  */
bool Z88_DevView::insertFspecList(QTreeWidgetItem *parent, QStringList &fspec_list, entryType d_type,
                                  const QString &fsize, const QString &fcreate_date, const QString &fmod_date )
{
    QStringList entry;
    QTreeWidgetItem *qt;

    int count;
    if(!fspec_list.isEmpty()){

        count = parent->childCount();
        /**
          * Search all the Children Here for Duplicates
          */
        for(int idx = 0; idx < count; idx++){
            if(parent->child(idx)->text(0) == fspec_list[0]){
                fspec_list.removeFirst();

                if(fspec_list.isEmpty()){
                    return false;
                }
                return insertFspecList(parent->child(idx), fspec_list, d_type, fsize, fcreate_date, fmod_date);
            }
        }

        entry << fspec_list[0];

        /**
          * Only insert size and date for files.
          */
        if(fspec_list.count() == 1){
            if(!fsize.isEmpty()){
                entry << fsize;
            }
            else{
                entry << " ";
            }

            if(!fcreate_date.isEmpty()){
                entry << fcreate_date;
            }
            else{
                entry << " ";
            }

            if(!fmod_date.isEmpty()){
                entry << fmod_date;
            }
            else{
                entry << " ";
            }
        }

        qt = new QTreeWidgetItem(entry, d_type);

        /**
          * Set the Icon for the file type
          */
        set_EntryIcon(qt, fspec_list[0], d_type);

        parent->addChild(qt);

        fspec_list.removeFirst();

        if(fspec_list.isEmpty()){
            return true;
        }
        return insertFspecList(qt, fspec_list, d_type, fsize, fcreate_date, fmod_date);
    }
    return false;
}

/**
  * Get the Fully qualified file name for the specified Item
  * @param item is the QTreeWidgetItem
  * @paramfspec is filled in file spec class.
  * @return the File Spec
  */
const Z88_Selection &Z88_DevView::getItemFspec(QTreeWidgetItem *item, Z88_Selection &fspec)const
{
    QString fname;
    fspec.m_fspec = m_devname + "/";

    bool rootdev = false;

    if(item){
        fspec.m_type = (item->type() == type_File) ? type_File : type_Dir;
    }
    else{
        rootdev = true;
        fspec.m_type = Z88_DevView::type_Dir;
    }

    /**
      * Build the Full filename by backing up to the top of the tree
      */
    while(item){
        fname.prepend(item->text(0));
        item = item->parent();
        if(!item)break;
        fname.prepend("/");
    }
    fspec.m_fspec += fname;

    /**
      * If the Selection is just the Z88 Storage device root,
      * Don't append a trailing '/'
      */
    if(!rootdev && fspec.m_type == Z88_DevView::type_Dir ){
        fspec.m_fspec += "/";
    }
    return fspec;
}

/**
  * Get a list of Sub Items below the specified item entry.
  * @param item is the top level selection item.
  * @param selections is the list to be filled in.
  * @return the list of selections.
  */
const QList<Z88_Selection> &Z88_DevView::getItemChildren(QTreeWidgetItem *item, const QString &parent, QList<Z88_Selection> &selections, bool depth_first = false) const
{
    QTreeWidgetItem *child;
    int childcount = item->childCount();


    for(int x=0; x < childcount; x++){
        child = item->child(x);

        QString par = parent + "/";
        par += child->text(0);

        Z88_Selection z88Selection(child, m_devname);

        z88Selection.setRelFspec(par);

        getItemFspec(child, z88Selection);

        if(!depth_first) {
            selections.append(z88Selection);
        }

        if(child->childCount()){
            getItemChildren(child, par, selections);
        }

        if(depth_first) {
            selections.append(z88Selection);
        }

    }
    return selections;
}

/**
  * Add an Icon based on the file type.
  * @param qt is the q tree widget item.
  * @param fname is the file name
  * @param d_type is the type of entry, dir or file
  */
void Z88_DevView::set_EntryIcon(QTreeWidgetItem *qt, const QString &fname, entryType d_type)
{
    QString ext;

    int idx = fname.lastIndexOf('.');

    if(idx > -1){
        ext= fname.mid(idx + 1,3);
    }

    /**
      * Set the Icon for the file type
      */
    if(d_type == type_Dir){
        qt->setIcon(0,QIcon(":/images/folder_icon"));
    }
    else{
        if(!QString::compare(ext, "bin",Qt::CaseInsensitive )){
            qt->setIcon(0,QIcon(":/images/bin_icon"));
            return;
        }
        if(!QString::compare(ext, "epr",Qt::CaseInsensitive )){
            qt->setIcon(0,QIcon(":/images/bin_icon"));
            return;
        }
        if(ext >= "0" && ext <= "63"){
            qt->setIcon(0,QIcon(":/images/bin_icon"));
            return;
        }
        if(!QString::compare(ext, "bas",Qt::CaseInsensitive )){
            qt->setIcon(0,QIcon(":/images/bas_icon"));
            return;
        }
        if(!QString::compare(ext, "txt",Qt::CaseInsensitive )){
            qt->setIcon(0,QIcon(":/images/txt_icon"));
            return;
        }
        if(!QString::compare(ext, "zip",Qt::CaseInsensitive )){
            qt->setIcon(0,QIcon(":/images/zip_icon"));
            return;
        }
        if(!QString::compare(ext, "cli",Qt::CaseInsensitive )){
            qt->setIcon(0,QIcon(":/images/cli_icon"));
            return;
        }
        qt->setIcon(0,QIcon(":/images/file_icon"));
    }
}

/**
  * Save all the Expanded Items and Collapse them.
  */
void Z88_DevView::SaveCollapseAll()
{
    int cnt = topLevelItemCount();

    m_ExpandedList.clear();

    while(cnt){
        QTreeWidgetItem *item(topLevelItem(cnt));
        if(item && item->isExpanded()){
            m_ExpandedList.append(item);
            item->setExpanded(false);
        }
        cnt--;
    }
}

/**
  * Restore all the Previously Collapsed Exmpanded Items.
  */
void Z88_DevView::RestoreCollapseAll()
{
    QListIterator<QTreeWidgetItem*> i(m_ExpandedList);

    while(i.hasNext()){
        i.next()->setExpanded(true);
    }
}

void Z88_DevView::dragEnterEvent(QDragEnterEvent *event)
{
    /**
      * Don't allow a drop of a Z88 File from itself
      */
    if(event->source() == this || !event->mimeData()->hasUrls()){
        event->ignore();
        return;
    }

    clearSelection();
    event->acceptProposedAction();
}

void Z88_DevView::dragMoveEvent(QDragMoveEvent *event)
{
    event->acceptProposedAction();
}

void Z88_DevView::dragLeaveEvent(QDragLeaveEvent *event)
{
    event->accept();
}

bool Z88_DevView::dropMimeData(QTreeWidgetItem *parent, int , const QMimeData *data, Qt::DropAction )
{
    /**
      * If a Directory or Item was selected.
      * Otherwise the Root device is selected.
      */
    if(parent){
        parent->setSelected(true);
    }    

    if(data->hasUrls()){
        QList<QUrl> *urlList = new QList<QUrl>(data->urls());
        QList<Z88_Selection> *z88_dest = new QList<Z88_Selection>(*getSelection(false));

        emit DropRequested(z88_dest, urlList);
        return true;
    }
    return false;
}

/**
  * Get the List of selected Files and Directories.
  * @param recurse set to true to get all the sub files within selected directories.
  * @return the list of fully qualified file names and directories.
  */
QList<Z88_Selection> *Z88_DevView::getSelection(bool recurse)
{
    typedef QTreeWidgetItem * qti_p;

    /**
      * if No files are selected, then selectall
      */
    if(recurse && selectedItems().isEmpty()){
        m_selChangeLock = true;
        SaveCollapseAll();
        selectAll();
        RestoreCollapseAll();
    }

    const QList<qti_p> &selections(selectedItems());
    QListIterator<qti_p> i(selections);

    m_Selections.clear();

    if(!selections.isEmpty()){
        while(i.hasNext()){
            qti_p item(i.next());
            Z88_Selection z88Selection(item, m_devname);

            m_Selections.append(z88Selection);

            if(recurse){
                /**
                  * Recurse the Directory tree if any
                  */
                getItemChildren(item, item->text(0), m_Selections);
            }
        }
    }
    else{
        /**
          * Nothing selected, so get the Device name
          */
        Z88_Selection z88Selection(NULL, m_devname);
        m_Selections.append(z88Selection);
    }

    if(m_selChangeLock){
        clearSelection();
        m_selChangeLock = false;
    }

    return &m_Selections;
}

bool Z88_DevView::set_FreeSpace(quint32 free_bytes)
{
    m_devFree = free_bytes;
    return true;  // for now
}

bool Z88_DevView::set_TotalSize(quint32 total_bytes)
{
    m_devSize = total_bytes;
    return true;
}

bool Z88_DevView::get_FreeSpace(quint32 &free_bytes, quint32 &tot_size)
{
    if(m_devFree != SZ_NOT_AVAIL){
        free_bytes = m_devFree;
        tot_size = m_devSize;
        return true;
    }
    return false;
}

bool Z88_DevView::get_TotalSize(quint32 &total_bytes)
{
    if(m_devFree != SZ_NOT_AVAIL){
        total_bytes = m_devSize;
        return true;
    }
    return false;
}
