/*********************************************************************************************

 EazyLink2 - Fast Client/Server Z88 File Management
 Copyright (C) 2012-2015 Gunther Strube (gstrube@gmail.com) & Oscar Ernohazy

 EazyLink2 is free software; you can redistribute it and/or modify it under the terms of the
 GNU General Public License as published by the Free Software Foundation;
 either version 2, or (at your option) any later version.
 EazyLink2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 See the GNU General Public License for more details.
 You should have received a copy of the GNU General Public License along with EazyLink2;
 see the file COPYING. If not, write to the
 Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

**********************************************************************************************/

#include <QMessageBox>
#include <QTimer>
#include <QTime>
#include <QDir>
#include <QFile>
#include <QSysInfo>
#include <QtGlobal>
#include <QStandardPaths>
#include <QtSerialPort/QSerialPort>

#include "protocoltest.h"
#include "z88comms.h"
#include "ui_protocoltest.h"
#include "console.h"
#include "crc32.h"

// This test uses EazyLink popdown protocol level 9 commands
const static int testProtocolLevel = 9;


ProtocolTestWindow::ProtocolTestWindow(Z88SerialPort &z88, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ProtocolTestWindow),
    z88(z88)
{
    ui->setupUi(this);

    console = new Console;
    console->setEnabled(false);
    setCentralWidget(console);

    console->setDocumentTitle("Testing serial port communication with Cambridge Z88");

    console->insertPlainText( "---------------------------------------------------------------------------\n" );
    console->insertPlainText( "This test requires a serial port connection to a Cambridge Z88\n" );
    console->insertPlainText( "with EazyLink popdown running and waiting for communication.\n" );
    console->insertPlainText( "---------------------------------------------------------------------------\n\n" );

    openTestSession();
}

void ProtocolTestWindow::statusMessage(QString msg)
{
    ui->statusBar->showMessage(msg);
}

ProtocolTestWindow::~ProtocolTestWindow()
{
    delete ui;
}


void ProtocolTestWindow::openTestSession()
{
    console->setEnabled(true);
    statusMessage(tr("Connected to %1").arg(z88.getPortName()));

    // start running the EazyLink popdown tests...
    QTimer::singleShot(500, this, SLOT(runEazylinkTesting()));
}



// run protocol testing, abort at first failure...
bool ProtocolTestWindow::testingSequence()
{
    bool status;
    QString protocolTestPath = QStandardPaths::writableLocation(QStandardPaths::DownloadLocation);
    QFile testHostFile;

    console->displayText("EazyLink Version = " + z88.getEazyLinkZ88Version() + ", protocol level = " +
                         QString("%1").arg(z88.getEazylinkPopdownProtocolLevel()) + "\n");

    if (z88.getEazylinkPopdownProtocolLevel() >= 4) {
        console->displayText("getZ88FreeMem() = " + z88.getZ88FreeMem() + "\n");
    }

    if (z88.getEazylinkPopdownProtocolLevel() >= 3) {
        QList<QByteArray> ramDefaults = z88.getRamDefaults();
        console->insertPlainText("getRamDefaults() = " + ramDefaults[0] + "\n");
    }

    // -----------------------------------------------------------------------
    if (z88.getEazylinkPopdownProtocolLevel() >= 4) {
        QList<QByteArray> z88Timestamp = z88.getZ88Time();
        if (z88Timestamp.length() == 2) {
            console->insertPlainText("getZ88Time() = " + z88Timestamp[0] + " " + z88Timestamp[1] + "\n");
        }
        status = z88.setZ88Time();
        console->insertPlainText("PC->Z88, setZ88Time() = " + QString(status == true ? "true": "false") + "\n");
        if (status == false) return false;

        z88Timestamp = z88.getZ88Time();
        if (z88Timestamp.length() == 2) {
            console->insertPlainText("Get updated Z88 time, getZ88Time() = " + z88Timestamp[0] + " " + z88Timestamp[1] + "\n");
        }
    }
    // -----------------------------------------------------------------------

    console->insertPlainText("translationOn() = " + QString( (status = z88.translationOn()) == true ? "true": "false") + "\n");
    if (status == false) return false;
    console->insertPlainText("translationOff() = " + QString( (status = z88.translationOff()) == true ? "true": "false") + "\n");
    if (status == false) return false;
    console->insertPlainText("linefeedConvOn() = " + QString( (status = z88.linefeedConvOn()) == true ? "true": "false") + "\n");
    if (status == false) return false;
    console->insertPlainText("linefeedConvOff() = " + QString( (status = z88.linefeedConvOff()) == true ? "true": "false") + "\n");
    if (status == false) return false;

    if (z88.getEazylinkPopdownProtocolLevel() >= 2) {
        console->insertPlainText("reloadTranslationTable() = " + QString( (status = z88.reloadTranslationTable()) == true ? "true": "false") + "\n");
        if (status == false) return false;
    }

    // -----------------------------------------------------------------------------------------------------------------------------------------
    // Device, directory & file browsing
    QByteArray devno;
    QList<QByteArray>::iterator i,j;
    QList<QByteArray> z88Devices = z88.getDevices();

    console->insertPlainText("getDevices() = ");
    for (i = z88Devices.begin(); i != z88Devices.end(); ++i)
        console->insertPlainText( *i + " ");
    console->insertPlainText("\n");

    if (z88.getEazylinkPopdownProtocolLevel() >= 3) {
        z88.deleteFileDir(":RAM.-/testdir1");

        console->insertPlainText("createDir(':RAM.-/testdir1') = " + QString( (status = z88.createDir(":RAM.-/testdir1")) == true ? "true": "false") + "\n");
        if (status == false) return false;
    }

    if (z88.getEazylinkPopdownProtocolLevel() >= 5) {
        for (int i=0; i<z88Devices.count(); i++) {
            devno = QByteArray(1,z88Devices[i].at(5));
            console->insertPlainText( QString("getZ88DeviceFreeMem(%1) = ").arg(z88Devices[i].at(5)) + z88.getZ88DeviceFreeMem(devno) + "\n");
        }
    }

    if (z88.getEazylinkPopdownProtocolLevel() >= 6) {
        for (i = z88Devices.begin(); i != z88Devices.end(); ++i) {
            console->insertPlainText( QString("getDeviceInfo(") + *i + ") = ");
            QList<QByteArray> devInfo = z88.getDeviceInfo(*i);
            if (devInfo.count() > 0) {
                console->insertPlainText( devInfo[0] + " " + devInfo[1] + "\n");
            } else {
                console->insertPlainText( "device info not available!\n");
                return false;
            }
        }
    }

    // get directories from Z88 Devices (root file system)
    for (i = z88Devices.begin(); i != z88Devices.end(); ++i) {
        console->insertPlainText( QString("getDirectories(") + *i + ") = ");
        QList<QByteArray> directories = z88.getDirectories(*i + "//*", status);
        for (j = directories.begin(); j != directories.end(); ++j) {
            console->insertPlainText( *j + " ");
        }
        console->insertPlainText("\n");
    }

    if (z88.getEazylinkPopdownProtocolLevel() >= 2) {
        // delete previously created directory (see above)
        console->insertPlainText("deleteFileDir(:RAM.-/testdir1) = " + QString( (status = z88.deleteFileDir(":RAM.-/testdir1")) == true ? "true": "false") + "\n");
        if (status == false) return false;
    }

    // get files from Z88 Devices (root file system)
    for (i = z88Devices.begin(); i != z88Devices.end(); ++i) {
        console->insertPlainText( QString("getFilenames(") + *i + ") = ");
        QList<QByteArray> files = z88.getFilenames(*i + "//*", status);
        for (j = files.begin(); j != files.end(); ++j) {
            console->insertPlainText( *j + " ");
        }
        console->insertPlainText("\n");
    }

    // -----------------------------------------------------------------------------------------------------------------------------------------

    // -----------------------------------------------------------------------------------------------------------------------------------------
    // File testing

    // try to send a binary file of 16K bytes containing the entire byte-range (0-255)...
    QByteArray fileContents = QByteArray(16384, 0);
    for (int i=0; i<fileContents.size(); i++) fileContents[i] = i % 256;

    Crc32 crc32 = Crc32().checksum(fileContents);
    QByteArray fileSize;

    // always delete "big" files first, before uploading / overwriting them (avoid wierd timeouts because Z88 must delete file first in GN_Opf API)
    if (z88.isFileAvailable(":RAM.-/uploadtest01.dat") == true) {
        console->insertPlainText("isFileAvailable(':RAM.-/uploadtest01.dat') = true\n");
        if (z88.getEazylinkPopdownProtocolLevel() >= 2) {
            // delete the test file before upload...
            statusMessage("Deleting 16K test-data ':RAM.-/uploadtest01.dat' file...");
            console->insertPlainText("deleteFileDir(':RAM.-/uploadtest01.dat') = " + QString( (status = z88.deleteFileDir(":RAM.-/uploadtest01.dat")) == true ? "true": "false") + "\n");
            if (status == false) return false;
        }
    }

    statusMessage("Sending 16K test-data to ':RAM.-/uploadtest01.dat'...");
    if (z88.sendFileData(":RAM.-/uploadtest01.dat", fileContents, false) == true) {
        console->insertPlainText( QString("sendFileData(':RAM.-/uploadtest01.dat') [%1 bytes] = true\n").arg(z88.getTotalFileTransferStream()));

        if (z88.isFileAvailable(":RAM.-/uploadtest01.dat") == true) {
            // perform remote operations of successfully uploaded file
            console->insertPlainText("isFileAvailable(':RAM.-/uploadtest01.dat') = true\n");

            fileSize = z88.getFileSize(":RAM.-/uploadtest01.dat");
            if (fileSize.length() > 0) {
                console->insertPlainText( QString("getFileSize(':RAM.-/uploadtest01.dat') = ") + fileSize + "\n");
            } else {
                console->insertPlainText("No file size was received!\n");
                return false;
            }

            QList<QByteArray> fileTD(z88.getFileDateStamps(":RAM.-/uploadtest01.dat"));
            if (fileTD.count() == 2) {
                console->insertPlainText( "getFileDateStamps(':RAM.-/uploadtest01.dat') = " + fileTD[0] + " " + fileTD[1] + "\n");

                status = z88.setFileDateStamps(":RAM.-/uploadtest01.dat",fileTD[0], fileTD[1]);
                if (status == true) {
                    console->insertPlainText( "setFileDateStamps(':RAM.-/uploadtest01.dat','"+ fileTD[0] + "','" + fileTD[1] +"') = true\n");
                } else {
                    console->insertPlainText( "setFileDateStamps(':RAM.-/uploadtest01.dat','"+ fileTD[0] + "','" + fileTD[1] +"') = false\n");
                    return false;
                }
            } else {
                console->insertPlainText("getFileDateStamps(':RAM.-/uploadtest01.dat'): No date stamps were received!\n");
                return false;
            }

            if (z88.getEazylinkPopdownProtocolLevel() >= 6) {
                statusMessage("CRC-32 of 16K test-data in ':RAM.-/uploadtest01.dat'...");
                QByteArray fileCrc32String = z88.getFileCrc32(":RAM.-/uploadtest01.dat", fileSize.toInt());
                if (fileCrc32String.length() > 0) {
                    console->insertPlainText( QString("CRC32 of test-data (before upload) = %1 \n").arg(crc32.getChecksum(), 0, 16).toUpper());

                    uint remotecrc32 = fileCrc32String.toUInt(&status,16);
                    console->insertPlainText( QString("getFileCrc32(':RAM.-/uploadtest01.dat') = ") + fileCrc32String + "\n");
                    console->insertPlainText("CRC32 validation = " + QString(remotecrc32 == crc32.getChecksum() ? "true": "false") + "\n");
                    if (remotecrc32 != crc32.getChecksum()) return false;
                } else {
                    console->insertPlainText("Non CRC32 checksum was received!\n");
                    return false;
                }
            }

            statusMessage("Receiving 16K test-data from Z88 to " + protocolTestPath);

            if (z88.receiveFile(":RAM.-/uploadtest01.dat", protocolTestPath, "uploadtest01.dat", true) == Z88SerialPort::rc_done ) {
                testHostFile.setFileName(QString(protocolTestPath).append("/").append("uploadtest01.dat"));

                console->insertPlainText("receiveFile(':RAM.-/uploadtest01.dat') -> " + protocolTestPath + " = true\n");
                status = crc32.getChecksum() == Z88Comms::hostFileCrc32( testHostFile.fileName() );
                console->insertPlainText("CRC32 validation = " + QString(status ? "true": "false") + "\n");
                testHostFile.remove();
                if (status == false) return false;
            } else {
                console->insertPlainText("receiveFile(':RAM.-/uploadtest01.dat') = false\n");
                return false;
            }

        } else {
            console->insertPlainText("isFileAvailable(':RAM.-/uploadtest01.dat') = false\n");
            return false;
        }
    } else {
        console->insertPlainText("sendFileData(':RAM.-/uploadtest01.dat') = false\n");
        return false;
    }

    if (z88.getEazylinkPopdownProtocolLevel() >= 2) {
        // delete the test file uploaded...
        statusMessage("Deleting 16K test-data ':RAM.-/uploadtest01.dat' file...");
        console->insertPlainText("deleteFileDir(':RAM.-/uploadtest01.dat') = " + QString( (status = z88.deleteFileDir(":RAM.-/uploadtest01.dat")) == true ? "true": "false") + "\n");
        if (status == false) return false;
    }

    // try to send a binary file of 512 bytes containing the entire byte-range (0-255)...
    // the entire data will be held in the file buffer of EazyLink popdown, before being flushed to Z88 RAM file
    // (this means no OS_MV is executed while data is received from serial port)
    fileContents = QByteArray(512, 0);
    for (int i=0; i<fileContents.size(); i++) fileContents[i] = i % 256;

    crc32 = Crc32().checksum(fileContents);

    statusMessage("Sending 512 bytes test-data to ':RAM.-/uploadtest02.dat'...");
    if (z88.sendFileData(":RAM.-/uploadtest02.dat", fileContents, false) == true) {
        z88.wait(500);
        console->insertPlainText( QString("sendFileData(':RAM.-/uploadtest02.dat') [%1 bytes] = true\n").arg(fileContents.size()));

        if (z88.isFileAvailable(":RAM.-/uploadtest02.dat") == true) {
            // perform remote operations of successfully uploaded file
            console->insertPlainText("isFileAvailable(':RAM.-/uploadtest02.dat') = true\n");

            fileSize = z88.getFileSize(":RAM.-/uploadtest02.dat");
            if (fileSize.length() > 0) {
                console->insertPlainText( QString("getFileSize(':RAM.-/uploadtest02.dat') = ") + fileSize + "\n");
            } else {
                console->insertPlainText("No file size was received!\n");
                return false;
            }

            console->insertPlainText( QString("getFileSize(':RAM.-/uploadtest02.dat') = ") + fileSize + "\n");

            if (z88.getEazylinkPopdownProtocolLevel() >= 6) {
                QByteArray fileCrc32String = z88.getFileCrc32(":RAM.-/uploadtest02.dat", fileSize.toInt());
                if (fileCrc32String.length() > 0) {
                    console->insertPlainText( QString("CRC32 of test-data (before upload) = %1\n").arg(crc32.getChecksum(), 0, 16));

                    uint remotecrc32 = fileCrc32String.toUInt(&status,16);
                    console->insertPlainText( QString("getFileCrc32(':RAM.-/uploadtest02.dat') = ") + fileCrc32String + "\n");
                    console->insertPlainText("CRC32 validation = " + QString(remotecrc32 == crc32.getChecksum() ? "true": "false") + "\n");
                    if (remotecrc32 != crc32.getChecksum())
                        return false;
                }
            }

            statusMessage("Receiving 512 bytes of test-data from Z88 to " + protocolTestPath);

            if (z88.receiveFile(":RAM.-/uploadtest02.dat", protocolTestPath, "uploadtest02.dat", true) == Z88SerialPort::rc_done ) {
                testHostFile.setFileName(QString(protocolTestPath).append("/").append("uploadtest02.dat"));
                console->insertPlainText("receiveFile(':RAM.-/uploadtest02.dat') = true\n");
                status = crc32.getChecksum() == Z88Comms::hostFileCrc32( testHostFile.fileName() );
                console->insertPlainText("CRC32 validation = " + QString(status ? "true": "false") + "\n");
                testHostFile.remove();
                if (status == false) return false;
            } else {
                console->insertPlainText("receiveFile(':RAM.-/uploadtest02.dat') = false\n");
                return false;
            }
        } else {
            console->insertPlainText("isFileAvailable(':RAM.-/uploadtest02.dat') = false\n");
            return false;
        }
    } else {
        console->insertPlainText("sendFileData(':RAM.-/uploadtest02.dat') = false\n");
        return false;
    }

    if (z88.getEazylinkPopdownProtocolLevel() >= 2) {
        // delete the two test files uploaded...
        statusMessage("Deleting 512 bytes test-data ':RAM.-/uploadtest02.dat' file...");
        console->insertPlainText("deleteFileDir(':RAM.-/uploadtest02.dat') = " + QString( (status = z88.deleteFileDir(":RAM.-/uploadtest02.dat")) == true ? "true": "false") + "\n");
        if (status == false) return false;
    }

    return true;
}


void ProtocolTestWindow::runEazylinkTesting()
{
    bool protocolTestStatus = false;
    QSerialPortInfo *portInfo = z88.getSerialPortDeviceInfo();
    console->setFocus();

    console->displayText("EazyLink Client " + QCoreApplication::applicationVersion() + ", ");
#if QT_VERSION >= 0x050400
    console->displayText("running on " + QSysInfo::prettyProductName() + " (" + QSysInfo::currentCpuArchitecture() + ")\n");
#else
    console->displayText("running on ");
#ifdef Q_OS_LINUX
    console->displayText("Linux");
#endif
#ifdef Q_OS_WIN32
    console->displayText("Windows (32bit)");
#endif
#ifdef Q_OS_WIN34
    console->displayText("Windows (64bit)");
#endif
#ifdef Q_OS_MAC
    console->displayText("MAC OSX");
#endif
#ifdef Q_PROCESSOR_X86_32
    console->displayText(" (x86)");
#endif
#ifdef Q_PROCESSOR_X86_64
    console->displayText(" (x86_64)");
#endif
#ifdef Q_PROCESSOR_ARM
    console->displayText(" (ARM)");
#endif

    console->displayText("\n");

#endif
    console->displayText("Using '"+z88.getPortName()+"' serial port to test EazyLink popdown communication at " + QString("%1").arg(z88.getSerialPortSpeed()) + " Baud\n");
    console->displayText("Serial Port hardware:\n");
    console->displayText("System location: " + portInfo->systemLocation() + "\n");
    console->displayText("Description: " + portInfo->description() + "\n");
    console->displayText("Manufacturer: " + portInfo->manufacturer() + "\n");
#if QT_VERSION >= 0x050300
    console->displayText("Serial Number: " + portInfo->serialNumber() + "\n");
#endif
    if (portInfo->hasProductIdentifier())
        console->displayText("Product Identifier: " + QString("%1").arg(portInfo->productIdentifier()) + "\n");
    if (portInfo->hasVendorIdentifier())
        console->displayText("Vendor Identifier: " + QString("%1").arg(portInfo->vendorIdentifier()) + "\n");
    console->displayText("\n");

    if (z88.helloZ88() == false) {
        console->displayText("helloZ88() reported 'No response'\n");
    } else {
        console->displayText("helloZ88() reported 'YES'\n");

        // run test-sequence using current speed as selected by main EazyLink.
        protocolTestStatus = testingSequence();
    }

    if ( (z88.isZ88Available() == true) && (testProtocolLevel > z88.getEazylinkPopdownProtocolLevel())) {
        console->insertPlainText("\n------------------------------------------------------------------------\n");
        console->insertPlainText( QString("This test is designed for EazyLink protocol level %1, but the popdown\n").arg(testProtocolLevel) );
        console->insertPlainText( QString("reported level %1. Limited testing has been done until level %1.\n").arg(z88.getEazylinkPopdownProtocolLevel()) );
        console->insertPlainText("------------------------------------------------------------------------\n");
    }

    console->insertPlainText( "\n---------------------------------------------------------------------------\n" );
    if (protocolTestStatus == true) {
        console->insertPlainText( "Protocol testing PASSED.\n" );
        statusMessage("Protocol testing PASSED.\n");
    } else {
        console->insertPlainText( "Protocol testing FAILED.\n" );
        statusMessage("Protocol testing FAILED.\n");
    }
    console->insertPlainText( "---------------------------------------------------------------------------\n" );
}
