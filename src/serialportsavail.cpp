/*********************************************************************************************

 EazyLink2 - Fast Client/Server Z88 File Management
 Copyright (C) 2012-2015 Gunther Strube (gstrube@gmail.com) & Oscar Ernohazy

 EazyLink2 is free software; you can redistribute it and/or modify it under the terms of the
 GNU General Public License as published by the Free Software Foundation;
 either version 2, or (at your option) any later version.
 EazyLink2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 See the GNU General Public License for more details.
 You should have received a copy of the GNU General Public License along with EazyLink2;
 see the file COPYING. If not, write to the
 Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

**********************************************************************************************/

#include <QtSerialPort/QSerialPortInfo>
#include "serialportsavail.h"


/**
  * Default constructor.
  */
SerialPortsAvail::SerialPortsAvail()
{
    get_portList();
}

/**
  * The Destructor
  */
SerialPortsAvail::~SerialPortsAvail(){}


/**
 * @return a list of serial port devices.
 */
const QStringList&
SerialPortsAvail::get_portList()
{
    m_portlist.clear();

    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) {
        m_portlist << info.portName();
    }

    return m_portlist;
}

/**
  * get the Fully qualified port name of the First available port.
  * @return the device name of the first port on the list or empty string if no ports available
  */
const QString &
SerialPortsAvail::getdefaultportName()
{
    static const QString noDefaultName = "";
    return (m_portlist.length() > 0) ? m_portlist[0]: noDefaultName;
}


/**
  * get the Fully qualified port name of the First available port.
  * @return the device name of the first port on the list
  */
bool SerialPortsAvail::portAvailable()
{
    return m_portlist.length() > 0;
}
