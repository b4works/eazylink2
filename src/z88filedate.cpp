#include "z88filedate.h"

/**
 * @brief Store Z88 create & update stamps as QDateTime objects
 * @param z88CreateDate
 * @param z88ModifiedDate
 */
Z88FileDate::Z88FileDate(QString z88CreateDate, QString z88ModifiedDate)
{
    createdTimestamp = Z88FileDate::toHostDate(z88CreateDate);
    updatedTimestamp = Z88FileDate::toHostDate(z88ModifiedDate);
}


/**
 * @brief Convert Z88 file stamp string format into QDateTime
 * @param z88FileDate string in "dd/mm/yyyy hh:mi:ss" format
 * @return
 */
QDateTime Z88FileDate::toHostDate(QString z88FileDate)
{
    QDate qdt;
    QTime qtm;

    /**
      * Convert the z88 date format to host format.
      */
    qdt = QDate::fromString(z88FileDate.mid(0,10), "dd/MM/yyyy");
    qtm = QTime::fromString(z88FileDate.mid(11), "hh:mm:ss");

    return QDateTime(qdt, qtm);
}


/**
 * @brief Convert from Host file date to Z88 date stamp string format
 * @param hostFileDate"
 * @return a string in "dd/mm/yyyy hh:mi:ss" format
 */
QString Z88FileDate::fromHostDate(QDateTime hostFileDate)
{
    QString z88Date = hostFileDate.date().toString("dd/MM/yyyy");
    QString z88Time = hostFileDate.time().toString("hh:mm:ss");
    return z88Date + " " + z88Time;
}


QDateTime Z88FileDate::getCreatedTimestamp() const
{
    return createdTimestamp;
}

QDateTime Z88FileDate::getUpdatedTimestamp() const
{
    return updatedTimestamp;
}


