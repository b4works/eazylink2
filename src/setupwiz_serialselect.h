/*********************************************************************************************

 EazyLink2 - Fast Client/Server Z88 File Management
 Copyright (C) 2012-2015 Gunther Strube (gstrube@gmail.com) & Oscar Ernohazy

 EazyLink2 is free software; you can redistribute it and/or modify it under the terms of the
 GNU General Public License as published by the Free Software Foundation;
 either version 2, or (at your option) any later version.
 EazyLink2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 See the GNU General Public License for more details.
 You should have received a copy of the GNU General Public License along with EazyLink2;
 see the file COPYING. If not, write to the
 Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

**********************************************************************************************/

#ifndef SETUPWIZ_SERIALSELECT_H
#define SETUPWIZ_SERIALSELECT_H

#include <QWizardPage>
#include "serialportsavail.h"

namespace Ui {
class SetupWiz_SerialSelect;
}

class SetupWizard;
class Z88Comms;

class SetupWiz_SerialSelect : public QWizardPage
{
    Q_OBJECT
    
public:
    explicit SetupWiz_SerialSelect(SetupWizard *setupWiz, QWidget *parent);
    ~SetupWiz_SerialSelect();
    
    void initializePage();
    void cleanupPage();
    bool validatePage();

    bool isComplete() const;

private slots:
    void RefreshComsList();
    void CustomButtonClicked(int which);

    void openTest_Start(int portIdx);
    void openTest_result(int portIdx, bool success);

protected:
    bool StartZ88Scan(const QStringList &portList, bool EzLink = false);
    void display_InitScanMsg();

private:

    void set_textMsg(const QString &msg = QString(""));

    /**
      * The Parent Main Wizard
      */
    SetupWizard *m_setupWizard;

    /**
      * The Comm Thread
      */
    Z88Comms *m_cthread;

    /**
      * Flag that gets checked, if the user performs a Raw Ascii port scan. (z88 search).
      */
    QAbstractButton *m_serTested;

    /**
      * The pointer to the auto-generated Graphics.
      */
    Ui::SetupWiz_SerialSelect *ui;
};

#endif // SETUPWIZ_SERIALSELECT_H
