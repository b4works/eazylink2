#ifndef PROTOCOLTESTWINDOW_H
#define PROTOCOLTESTWINDOW_H

#include <QMainWindow>
#include "z88serialport.h"


namespace Ui {
class ProtocolTestWindow;
}

class Console;

class ProtocolTestWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit ProtocolTestWindow(Z88SerialPort &z88, QWidget *parent = 0);
    ~ProtocolTestWindow();

private slots:
    void openTestSession();
    void runEazylinkTesting();

private:
    void initActionsConnections();
    bool testingSequence();
    void statusMessage(QString msg);

private:
    Ui::ProtocolTestWindow *ui;
    Console *console;
    Z88SerialPort &z88;
};

#endif // PROTOCOLTESTWINDOW_H
