/*********************************************************************************************

 EazyLink2 - Fast Client/Server Z88 File Management
 Copyright (C) 2012-2015 Gunther Strube (gstrube@gmail.com) & Oscar Ernohazy

 EazyLink2 is free software; you can redistribute it and/or modify it under the terms of the
 GNU General Public License as published by the Free Software Foundation;
 either version 2, or (at your option) any later version.
 EazyLink2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 See the GNU General Public License for more details.
 You should have received a copy of the GNU General Public License along with EazyLink2;
 see the file COPYING. If not, write to the
 Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

**********************************************************************************************/

#include <QDebug>
#include "setupwiz_modeselect.h"
#include "ui_setupwiz_modeselect.h"

/**
  * Setup Wizard Mode Select Page.
  */
SetupWiz_ModeSelect::SetupWiz_ModeSelect(QWidget *parent) :
    QWizardPage(parent),
    ui(new Ui::SetupWiz_ModeSelect)
{

    QFont font("vera");
    font.setStyleHint(QFont::SansSerif);
    font.setStyle(QFont::StyleNormal);
    font.setPixelSize(11);
    setFont(font);

    ui->setupUi(this);

    static const QString textmsg = "The Eazylink2 Client supports two Z88 Popdowns:\n"
                                   "The Full featured Eazylink, and the basic Imp-Export.\n\n";

    ui->OpModeText->setText(textmsg);

    registerField("modeEzlink", ui->ezlink_btn);
    registerField("modeImpExp", ui->impexp_btn);
}

SetupWiz_ModeSelect::~SetupWiz_ModeSelect()
{
    delete ui;
}
