/*********************************************************************************************

 EazyLink2 - Fast Client/Server Z88 File Management, Z88 EazyLink Communication Protocol
 Copyright (C) 2011-2015 Gunther Strube (gstrube@gmail.com)

 EazyLink2 is free software; you can redistribute it and/or modify it under the terms of the
 GNU General Public License as published by the Free Software Foundation;
 either version 2, or (at your option) any later version.
 EazyLink2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 See the GNU General Public License for more details.
 You should have received a copy of the GNU General Public License along with EazyLink2;
 see the file COPYING. If not, write to the
 Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

**********************************************************************************************/

#ifndef Z88SERIALPORT_H
#define Z88SERIALPORT_H

#include <QtCore/QTextStream>
#include <QtCore/QDebug>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QElapsedTimer>


/**
  * The Z88 Serial port Communication Class.
  * Implements the various file transfer, and information set/get operations.
  */
class Z88SerialPort : public QObject
{
    Q_OBJECT

public:
    Z88SerialPort();
    ~Z88SerialPort();

    enum retcode{
        rc_ok,
        rc_done,
        rc_timeout,
        rc_inv,
        rc_eof,
        rc_busy
    };

    QSerialPortInfo *getSerialPortDeviceInfo();
    void wait(const int timeoutMs);                                         // wait and do nothing for X ms...
    bool openSerialPort();
    bool openSerialPort(QString pName, qint32 speed, bool hardwareFlowControl);
    void closeSerialport();
    void setPortName(QString pName);                                        // define the serial port device name
    bool isOpen();                                                          // Return Port Open state.
    bool isZ88Available();                                                  // Has communication been established with EazyLink popdown?
    bool isTransmitting();                                                  // is a Z88 protocol command executing?

    bool helloZ88();                                                        // poll for "hello" to Z88
    bool useXonXoff();                                                      // switch to Xon/Xoff software handshaking on the Z88
    bool useRtsCts();                                                       // switch to Rts/CTS hardware handshaking on the Z88
    bool use38400bps();                                                     // switch to 38.400 bps on the Z88
    bool quitZ88();                                                         // quit EazyLink popdown on Z88
    bool translationOn();                                                   // enable byte translations during transfer
    bool translationOff();                                                  // disable byte translations during transfer
    bool linefeedConvOn();                                                  // enable linefeed conversions during transfer
    bool linefeedConvOff();                                                 // disable linefeed conversions during transfer
    bool reloadTranslationTable();                                          // remote reload translation table on Z88 EazyLink popdown
    bool setZ88Time();                                                      // set Z88 date/time using PC local time
    bool isFileAvailable(const QString &fileName);                          // ask if file exists on Z88 filing system
    bool createDir(const QString &pathName);                                // create directory on the Z88 filing system
    bool deleteFileDir(const QString &fileName);                            // delete a file / directory on the Z88 filing system
    bool renameFileDir(const QString &pathName, const QString &fileName);   // rename a file / directory on the Z88 filing system
    bool syncZ88Time();                                                     // synchronize Z88 time with Desktop time, if necessary
    bool setFileDateStamps(const QString &filename,
                           QByteArray createDate,
                           QByteArray updateDate);                          // set Create & Update date stamps of Z88 file
    bool uploadHostFile(const QString &z88Filename, QString hostFilename, bool lfConvProtLvl9);        // send a file to Z88 using EazyLink protocol
    bool sendFileData(const QString &z88Filename, QByteArray &fileContents, bool lfConvProtLvl9);
    bool sendFileDataCrc(const QString &z88Filename, QByteArray &fileContents, bool lfConvProtLvl9);
    bool sendImpExpFileData(QByteArray fileDataStream);

    bool impExpReceiveFiles(const QString &hostPath);                       // receive Z88 files from Imp/Export popdown
    retcode receiveFile(const QString &z88Filenames,
                         const QString &hostpath,
                         const QString &destFspec,
                         bool destisDir);                                   // receive one or more files from Z88 to host using EazyLink protocol

    QByteArray prepareImpExpFile(const QString &z88Filename, const QString hostFilename); // send a file to Z88 using Imp/Export protocol
    QByteArray getEazyLinkZ88Version();                                     // receive string of EazyLink popdown version and protocol level
    QByteArray getZ88FreeMem();                                             // receive string of Z88 All Free Memory
    QByteArray getZ88DeviceFreeMem(QByteArray device);                      // receive string of Free Memory from specific device
    QByteArray getFileSize(const QString &fileName);                        // receive string of Z88 File size
    QByteArray getFileCrc32(const QString &fileName, int z88FileSize);      // receive string of Z88 File CRC-32
    QList<QByteArray> getDevices();                                         // receive a list of Z88 Storage Devices
    QList<QByteArray> getZ88Time();                                         // receive string of current Z88 date & time
    QList<QByteArray> getRamDefaults();                                     // receive default RAM & default Directory from Z88 Panel popdown
    QList<QByteArray> getDirectories(const QString &path, bool &retc);      // receive a list of Z88 Directories in <path>
    QList<QByteArray> getFilenames(const QString &path, bool &retc);        // receive a list of Z88 Filenames in <path>
    QList<QByteArray> getFileDateStamps(const QString &fileName);           // receive Create & Update date stamps of Z88 file
    QList<QByteArray> getDeviceInfo(const QString &deviceName);             // receive Device Information (free space / total size)

    QString getEazylinkPopdownVersion();                                    // get EazyLink popdown version, for example "5.2"
    QString getPortName();                                                  // get device name of current serial port.
    QString getErrorString();
    int getEazylinkPopdownProtocolLevel();                                  // get EazyLink popdown communication protocol level, for example "7"
    int getSerialPortSpeed();                                               // get the current speed (BPS) of openen Serial Port

    bool        isBytesAvailable(const int timeOutSeconds);                 // Listen max X seconds for incoming bytes on serial port

    int getTotalFileTransferStream() const;
    qint64 getFileBufferFlushTime() const;

private slots:
    void        handleError(QSerialPort::SerialPortError error);
    void        handleBytesWritten(qint64 bytes);
    void        handleReadyRead();

signals:
    void impExpRecFilename(const QString &fname);
    void impExpRecFile_Done(const QString &fname);
    void totalBytesRead(int bytes);
    void totalBytesWritten(int bytes);

private:

    QSerialPort *port;                                      // the serial port device class
    QSerialPortInfo *portInfo;                              // information about the opened serial port

    bool       portOpenStatus;                              // status of opened port; true = opened, otherwise false for closed
    bool       z88AvailableStatus;                          // status of Z88 availability; true = accessed successfully, otherwise false
    bool       transmitting;                                // a transmission is current ongoing

    bool       bytesSent;
    int        totalBytesToSend;
    int        totalBytesSent;

    int        totalFileTransferStream;                     // total amount of file bytes streamed to/from Z88
    qint64     fileBufferFlushTime;                   // time (in ms) to transfer latest file to/from Z88.

    QByteArray response;                                    // collected response data stream from the handleReadyRead() method

    QString    eazylinkVersion;                             // cached string, when getEazyLinkZ88Version() method is called
    int        eazylinkProtocolLevel;                       // cached integer, when getEazyLinkZ88Version() method is called

    QString    portName;                                    // the default platform serial port device name
    QByteArray synchEazyLinkProtocol;                       // 1,1,2    constant
    QByteArray escEsc;                                      // ESC ESC  constant
    QByteArray esc1b;                                       // ESC B 1B constant
    QByteArray escB;                                        // ESC B    constant
    QByteArray escN;                                        // ESC N    constant
    QByteArray escE;                                        // ESC E    constant
    QByteArray escF;                                        // ESC F    constant
    QByteArray escY;                                        // ESC Y    constant
    QByteArray escZ;                                        // ESC Z    constant
    QByteArray escCrcStart;                                 // ESC [    constant
    QByteArray escCrcEnd;                                   // ESC ]    constant
    QByteArray helloCmd;                                    // ESC a    EazyLink V4.4 (protcol level 01) Hello
    QByteArray versionCmd;                                  // ESC v    EazyLink V4.5 (protcol level 01) EazyLink Server Version and protocol level
    QByteArray quitCmd;                                     // ESC q    EazyLink V4.4 (protcol level 01) Quit
    QByteArray devicesCmd;                                  // ESC h    EazyLink V4.4 (protcol level 01) Device
    QByteArray directoriesCmd;                              // ESC d    EazyLink V4.4 (protcol level 01) Directory
    QByteArray filesCmd;                                    // ESC n    EazyLink V4.4 (protcol level 01) Files
    QByteArray transOnCmd;                                  // ESC t    EazyLink V4.4 (protcol level 01) Translation ON
    QByteArray transOffCmd;                                 // ESC T    EazyLink V4.4 (protcol level 01) Translation OFF
    QByteArray lfConvOnCmd;                                 // ESC c    EazyLink V4.4 (protcol level 01) Linefeed Conversion ON
    QByteArray lfConvOffCmd;                                // ESC C    EazyLink V4.4 (protcol level 01) Linefeed Conversion OFF
    QByteArray receiveFilesCmd;                             // ESC s    EazyLink V4.4 (protcol level 01) Receive one or more files from Z88
    QByteArray sendFilesCmd;                                // ESC b    EazyLink V4.4 (protcol level 01) Send one or more files from Z88
    QByteArray fileExistCmd;                                // ESC f    EazyLink V4.5 (protcol level 01) File exists
    QByteArray fileSizeCmd;                                 // ESC x    EazyLink V4.5 (protcol level 01) Get file size
    QByteArray fileDateStampCmd;                            // ESC u    EazyLink V4.5 (protcol level 01) Get create and update date stamp of Z88 file
    QByteArray setFileTimeStampCmd;                         // ESC U    EazyLink V4.5 (protcol level 01) Set create and update date stamp of Z88 file
    QByteArray deleteFileDirCmd;                            // ESC r    EazyLink V4.6 (protcol level 02) Delete file/dir on Z88.
    QByteArray reloadTraTableCmd;                           // ESC z    EazyLink V4.6 (protcol level 02) Reload Translation Table
    QByteArray ramDefaultCmd;                               // ESC g    EazyLink V4.7 (protcol level 03) Get Z88 RAM defaults
    QByteArray renameFileDirCmd;                            // ESC w    EazyLink V4.7 (protcol level 03) Rename file/directory on Z88
    QByteArray createDirCmd;                                // ESC y    EazyLink V4.7 (protcol level 03) Create directory path on Z88
    QByteArray getZ88TimeCmd;                               // ESC e    EazyLink V4.8 (protcol level 04) Get Z88 system Date/Time
    QByteArray setZ88TimeCmd;                               // ESC p    EazyLink V4.8 (protcol level 04) Set Z88 System Clock using PC system time
    QByteArray freeMemoryCmd;                               // ESC m    EazyLink V4.8 (protcol level 04) Get free memory for all RAM cards
    QByteArray freeMemDevCmd;                               // ESC M    EazyLink V5.0 (protcol level 05) Get free memory for specific device
    QByteArray crc32FileCmd;                                // ESC i    EazyLink V5.2 (protcol level 06) Get CRC-32 of specified Z88 file
    QByteArray devInfoCmd;                                  // ESC O    EazyLink V5.2 (protcol level 06) Device Information
    QByteArray changeSpeedCmd;                              // ESC L    EazyLink V5.3 (protcol level 08) Change to 38400 bps
    QByteArray useXonXoffCmd;                               // ESC X    EazyLink V5.3 (protcol level 08) Switch to Xon/Xoff software handshaking
    QByteArray useRtsCtsCmd;                                // ESC Y    EazyLink V5.3 (protcol level 08) Switch to Rts/Cts hardware handshaking
    QElapsedTimer timeSinceLastReceivedByte;                // Measure elapsed time when no bytes are received from the serial port

    bool        sendBytes(QByteArray byteSequence, int timeoutMs = 10000);         // Send byte(s) to the Z88
    bool        synchronize();                              // Synchronize with Z88 before sending command
    bool        waitforResponse(const int timeoutMs, QByteArray endOfResponse);       // fetch response from Z88, wait max timeoutMs milliseconds
    bool        sendCommand(QByteArray cmd, int timeoutMs = 10000);                // Transmit ESC command to Z88
    bool        receiveListItems(QList<QByteArray> &list, int timeoutMs = 10000);  // Receive list of items (eg. devices, directories, filenames)
    char        xtod(char c);                               // hex to integer nibble function
    QByteArray  createImpExpFileData(const QString &z88Filename, QByteArray rawFileData);
    QByteArray  convert2z88TextFile(const QByteArray &fileContents);
};

#endif // Z88SERIALPORT_H
