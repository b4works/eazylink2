/*********************************************************************************************

 EazyLink2 - Fast Client/Server Z88 File Management
 Copyright (C) 2012-2015 Gunther Strube (gstrube@gmail.com) & Oscar Ernohazy

 EazyLink2 is free software; you can redistribute it and/or modify it under the terms of the
 GNU General Public License as published by the Free Software Foundation;
 either version 2, or (at your option) any later version.
 EazyLink2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 See the GNU General Public License for more details.
 You should have received a copy of the GNU General Public License along with EazyLink2;
 see the file COPYING. If not, write to the
 Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

**********************************************************************************************/

#include <QTextStream>
#include <QDebug>
#include "setupwiz_finalpage.h"
#include "ui_setupwiz_finalpage.h"
#include "setupwizard.h"
#include "prefrences_dlg.h"

/**
  * The Final Page of the Setup Wizard.
  * @param setupWiz is the Root Setup Wizard.
  */
SetupWiz_FinalPage::SetupWiz_FinalPage(SetupWizard *setupWiz, QWidget *parent) :
    QWizardPage(parent),
    m_setupWizard(setupWiz),
    ui(new Ui::SetupWiz_FinalPage)
{

    QFont font("vera");
    font.setStyleHint(QFont::SansSerif);
    font.setStyle(QFont::StyleNormal);
    font.setPixelSize(11);
    setFont(font);

    ui->setupUi(this);
}

SetupWiz_FinalPage::~SetupWiz_FinalPage()
{
    delete ui;
}

/**
  * Callback event that gets called if user hits 'back'
  */
void SetupWiz_FinalPage::cleanupPage()
{
    /**
      * Make the custome scan button invisible.
      */
    wizard()->setOption(QWizard::HaveCustomButton2, true);
}

/**
  * Callback event that happens when user hits 'next' and enters this page
  */
void SetupWiz_FinalPage::initializePage()
{
    QString msg;
    QTextStream omsg(&msg);

#ifndef Q_OS_MAC
    static const QString btn_name("Finish");
#else
    static const QString btn_name("Done");
#endif

    if(field("modeEzlink").toBool()){
        omsg << "You have successfully configured Eazylink2 to optimally communicate with the Z88 "
                "using the Eazylink popdown." << Qt::endl;

    }
    else{
        omsg << "You have successfully configured Eazylink2 to communicate with the Z88 using the Imp-Export "
                "Z88 popdown." << Qt::endl;

        /**
          * Check to see if the User Used the Scan for Z88 Option.
          * Then Remind them to close the Terminal.
          */
        if(field("rawTested").toBool()){

            omsg << Qt::endl
                 << "***NOTE: Make sure you exit the Terminal on the Z88 "
                    "before you try using Imp-Export." << Qt::endl << Qt::endl;

            omsg << "(To Exit Z88 Terminal: Press <Shift-ENTER>)." << Qt::endl;
        }
    }

    omsg << Qt::endl <<  Qt::endl
         << "Click \'" << btn_name << "\' to save settings and start using Eazylink2." << Qt::endl;

    ui->FinalMsg->setText(msg);
}

/**
  * Save the Setting into the Prefs panel.
  */
bool SetupWiz_FinalPage::validatePage()
{
    /**
      * Setup the Preferences based on the selected port, and the Type of protocol.
      */
     m_setupWizard->get_prefsDialog()->WriteWizardCfg(m_setupWizard->getPort(),
                                                      field("modeEzlink").toBool());
    return true;
}


